const express = require("express")
const PORT = process.env.PORT || 3001
var app = express()
app.get("/myApi",function(request,response){
    response.send([{message: "FIRST_MESSAGE"}])
})
app.listen(PORT, function () {
    console.log("Started application on port %d", PORT)
});